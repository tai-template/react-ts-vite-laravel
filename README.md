# About
This is a template project `React + Laravel + Typescript + Vite` and some other libraries like:
+ Bootstrap 5
+ Axios
# After download
```
cd <path-project>
composer install
npm install
npm run dev
php artisan serve
```

Read more: https://lazycodet.com/3/read/459

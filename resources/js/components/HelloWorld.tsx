import React, { useState } from 'react';

export default function HelloWorld(): JSX.Element {
    const [count, setCount] = useState<number>(0);
    
    function handleClick(): void {
        setCount(count + 1);
    }

    return (
        <div className="d-flex justify-content-center align-items-center" style={{height: "100vh"}}>
            <button onClick={handleClick} className="btn btn-primary p-3">{count}</button>
        </div>
    );
}

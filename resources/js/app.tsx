import './bootstrap'
import '../css/app.css'
import React from 'react';
import ReactDOM from 'react-dom/client';		
import HelloWorld from './components/HelloWorld';
import { BrowserRouter } from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('app')!).render(	
    <BrowserRouter>
        <HelloWorld />		
    </BrowserRouter>	
);